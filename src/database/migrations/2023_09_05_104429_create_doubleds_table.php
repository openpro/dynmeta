<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('doubleds', function (Blueprint $table) {
            $table->uuid('id')->primary();
            //$table->uuid('data_handlers_id');
            $table->string('ikey');
            $table->double('dvalue');
            $table->uuidMorphs('tag');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('doubleds');
    }
};
