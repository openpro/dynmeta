<?php

namespace Openpro\Dynmeta\Traits;

use Openpro\Dynmeta\Model\DataHandler;
use Illuminate\Database\Eloquent\Builder;
use Openpro\Dynmeta\Model\StringD;
use Openpro\Dynmeta\Model\IntegerD;
use Openpro\Dynmeta\Model\Doubled;

trait HasTag {
    
    public function scopeSetTag(Builder $query, $array){

        $data = $query->getModel()->getAttributes();
        $this->storeit($query->from, $data['id'], $array);
     
    }

    public function scopeUpdateTag(Builder $query, $array){

        $data = $query->getModel()->getAttributes();
        // dd($data);
        // dd($array);
        $this->storeitin($query->from, $data['id'], $array);
     
    }

    public function scopeGetTagById(Builder $query, $id){
        
        $post = DataHandler::with(['stringd','IntegerD','doubled'])->where('ikey',$id)->where('itype',$query->from)->get();
        return $post;

    }

    public function scopeSetEverythingAsTag(Builder $query, $id = 'id'){

        $data = $query->getModel()->getAttributes();
        $this->storeit($query->from, $data[$id], $data); //what if user want to use somethig else but not default AI() id.

    }

    public function scopeGetAllTag(Builder $query){
        
        $post = DataHandler::with(['stringd','IntegerD','doubled'])->where('itype',$query->from)->get();        
        return $post;

    }

    public function scopeGetAllTags(Builder $query){
        // UNDER STUDY || NOT READY TO USE.
        $post = DataHandler::whereHasMorph('tag', function ($query) {
            $query->where('price', '=', 12.03 );
        })->get();
        return $post;

    }

    private function storeit($itype, $ikey, $array){

        $datahandler = new DataHandler;
        $datahandler->itype = $itype;
        $datahandler->ikey = $ikey;
        $datahandler->save();
        
        foreach($array as $key=>$name){

            if( is_scalar( $name ) ){
                if( is_string( $name ) ){

                    $type = 'string';
                    $stringd = new StringD;
                    $stringd->ikey = $key;
                    $stringd->dvalue = $name;
                    $datahandler->stringd()->save($stringd);

                }else if( is_int( $name ) ){

                    $type = 'int';
                    $integer_d_s = new IntegerD;
                    $integer_d_s->ikey = $key;
                    $integer_d_s->dvalue = $name;
                    $datahandler->integerd()->save($integer_d_s);

                }else if( is_float( $name ) ){

                    $type = 'float';
                    $doubled = new Doubled;
                    $doubled->ikey = $key;
                    $doubled->dvalue = $name;
                    $datahandler->doubled()->save($doubled);

                }else if( is_double( $name ) ){
                    $type = 'float';
                }else if( is_bool( $name ) ){
                    $type = 'bool';
                    $name = intval( $name );
                }
            }

        }


    }


    
    private function storeitin($itype, $ikey, $array){

        $datahandler = DataHandler::where('itype', $itype)->where('ikey', $ikey)->get();

        $tag_id = $datahandler['0']['id'];
        echo $tag_id;       

        foreach($array as $key=>$name){
            if( is_scalar( $name ) ){
                if( is_string( $name ) ){
                    
                    $type = 'string';
                    $stringd = StringD::where('tag_id',$tag_id)->where('ikey',$key)->update(['dvalue'=> $name]);
                    
                }else if( is_int( $name ) ){

                    $type = 'int';
                    $integer_d_s = IntegerD::where('tag_id',$tag_id)->where('ikey',$key)->update(['dvalue'=> $name]);
                    
                }else if( is_float( $name ) ){

                    $type = 'float';
                    $doubled = Doubled::where('tag_id',$tag_id)->where('ikey',$key)->update(['dvalue'=> $name]);
                    
                }else if( is_double( $name ) ){
                    $type = 'float';
                }else if( is_bool( $name ) ){
                    $type = 'bool';
                    $name = intval( $name );
                }
            }
        }
    }

    

}