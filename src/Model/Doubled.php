<?php

namespace Openpro\Dynmeta\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;

class Doubled extends Model
{
    use HasFactory;
    use HasUuids;
    protected $hidden = ['id','tag_type','tag_id','created_at', 'updated_at'];

    protected $casts = ['dvalue' => 'double'];
    protected $fillable = ['ikey','dvalue'];


    public function tag(){
        return $this->morphTo();
    }

}
