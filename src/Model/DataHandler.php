<?php

namespace Openpro\Dynmeta\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;

class DataHandler extends Model
{
    use HasFactory;
    use HasUuids;

    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = ['itype','ikey'];

    public function integerd()
    {
        return $this->morphMany(IntegerD::class,'tag');
    }

    public function doubled()
    {
        return $this->morphMany(Doubled::class,'tag');
    }

    public function stringd()
    {
        return $this->morphMany(StringD::class,'tag');
    }

}
